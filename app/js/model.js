class Model {
  constructor(config = {}) {
    this.Store = config.storage || localStorage;
  }
  get list() {
    try {
      let str = this.Store.getItem('list');
      if (str === null || !str) return {};
      const store = JSON.parse(str);
      return store === null ? {} : store;
    } catch (e) {
      return this._error(e);
    }
  }
  set list(list) {
    this.Store.setItem('list', JSON.stringify(list));
  }
  addItem(text) {
    let store = this.list;
    let item = {
      text,
      id: this._getNewId
    }
    store[item.id] = item.text;
    this.list = store;
    return item;
  }
  editItem(item) {
    let store = this.list;
    store[item.id] = item.text;
    this.list = store;
    return true;
  }
  removeItem(id) {
    if (typeof id === 'object') id = id.id;
    let store = this.list;
    delete store[id];
    this.list = store;
    return true;
  }
  get _getNewId() {
    return ((Math.random() * 100) + '').substr(0, 6);
  }
  _error(text) {
    console.error(text);
  }
}
export default Model;