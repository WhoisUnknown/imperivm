import Controller from './controller.js';
import View from './view.js';

const view = new View('#app');
const ctr = new Controller(view);