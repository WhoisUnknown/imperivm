import Model from './model.js'; //require('./view');
class View {
  constructor($e) {
    if (!$e) return this._error('Bad params in constructor');
    this.rootElement = this.firstElement($e);
    this.model = new Model();
    this._createFirstHtml();
  }
  addItem(item) {
    let li = this._itemListForm(item);
    this.ul.appendChild(li);
  }
  createItem(text) {
    let res = this.model.addItem(text);
    if (!res) return this._error('create error');
    this.addItem(res);
  }
  removeItem(id) {
    let res = this.model.removeItem(id);
    if (!res) return this._error('remove error');
    let deleteElement = this.firstElement('[data-id="' + id + '"]');
    if (deleteElement === null) return this._error('not found deleteElement');
    this.ul.removeChild(deleteElement);
  }
  toggleForm(id) {
    let $el = this.firstElement('[data-id="' + id + '"]');
    let typeEdit = this.firstElement('.task-label', $el).getAttribute('contenteditable');
    typeEdit = typeEdit === 'true' ? true : false;
    let $label = this.firstElement('.task-label', $el);
    let res = this.model.editItem({
      id: id,
      text: $label.innerHTML
    });
    if (!res) return _error('edit error');
    this.firstElement('.task-label', $el).setAttribute('contenteditable', !typeEdit);
    this.firstElement('[data-btn-type="edit"]', $el).innerHTML = typeEdit ? 'Редактировать' : 'Сохранить';
  }
  _createFirstHtml(id) {
    this.ul = this._createElement({
      tag: 'ul',
      html: '',
      class: 'row TodoList col-12'
    });
    this.rootElement.className = 'col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2'
    this.rootElement.innerHTML = '';
    this._createButtonCreateForm();
    this.rootElement.appendChild(this.ul);
    let list = this.model.list;
    for (let key in list) {
      this.addItem({
        id: key,
        text: list[key]
      });
    }
  }
  _itemListForm(item) {
    let li = this._createElement({
      tag: 'li',
      html: '',
      class: "row col-12",
      attr: {
        'data-id': item.id
      }
    });
    li.appendChild(this._createElement({
      tag: 'div',
      html: item.text,
      class: "task-label col-10 col-sm-8",
    }));

    let btns = this._createElement({
      tag: 'div',
      class: "col-10 offset-1 offset-sm-0 col-sm-4 btns",
    });
    li.appendChild(btns);
    btns.appendChild(this._buttonForm({
      id: item.id,
      text: 'Редактировать',
      type: 'edit',
      class: 'btn btn-secondary col-sm-12'
    }))
    btns.appendChild(this._buttonForm({
      id: item.id,
      text: 'Удалить',
      type: 'delete',
      class: 'btn btn-danger col-sm-12'
    }))

    return li;

  }
  _buttonForm(item) {
    return this._createElement({
      tag: 'button',
      html: item.text,
      class: item.class,
      attr: {
        'data-id-btn': item.id,
        'data-btn-type': item.type,
      }
    });
  }
  _createButtonCreateForm() {
    let $div = this._createElement({
      tag: 'div',
      class: 'row create-block col-4 offset-4'
    })
    this.rootElement.appendChild($div);
    $div.appendChild(this._buttonForm({
      text: 'Создать',
      type: 'create',
      class: 'btn btn-secondary create-btn col-12'
    }));
  }
  _createElement(params) {
    if (typeof params === 'string') return document.createElement(params);
    else if (typeof params !== 'object') return this._error('params is not object');
    let e = document.createElement(params.tag);
    if (params.class) e.className = params.class;
    if (params.html) e.innerHTML = params.html;
    if (typeof params.attr === 'object') {
      for (let key in params.attr) e.setAttribute(key, params.attr[key]);
    }
    return e;
  }
  firstElement($selector, $root = document) {
    return $root.querySelectorAll($selector)[0] || null;
  }
  _error(msg) {
    console.error(msg);
  }
}
export default View;