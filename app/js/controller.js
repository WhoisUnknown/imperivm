class Controller {
  constructor(view) {
    document.addEventListener("DOMContentLoaded", (event) => {
      this.view = view;
      this._ListenerClickBtn();
    });
  }
  _ListenerClickBtn() {
    this.view.rootElement.addEventListener("click", $event => {
      let type = $event.target.getAttribute('data-btn-type');
      let id = $event.target.getAttribute('data-id-btn');
      switch (type) {
        case 'edit':
          {
            this.view.toggleForm(id);
            break;
          }
        case 'delete':
          {
            this.view.removeItem(id);
            break;
          }
        case 'create':
          {
            this.view.createItem(prompt('Введите таск'));
          }
          break;
        default:
      }
    })
  }
}
export default Controller;